<?php

/**
 * Created by PhpStorm.
 * User: sven
 * Date: 3/05/2017
 * Time: 15:30
 */
class People extends CI_Model
{

    public $first_name;
    public $last_name;
    public $email;

    public function __construct()
    {
        parent::__construct();

        // Load database library
        $this->load->database();
    }

    /**
     * Function to create a person
     */
    public function insert_entry($first_name, $last_name, $email)
    {
        $this->first_name   = ucwords(strtolower($first_name));
        $this->last_name    = ucwords(strtolower($last_name));
        $this->email        = strtolower($email);

        $this->db->insert('PEOPLE', $this);
    }

    /**
     * Function to get a person based of its PERSON_ID
     *
     * @param $person_id
     * @return mixed
     */
    public function get_entry($person_id)
    {
        $this->db->select("p.PERSON_ID, p.FIRST_NAME, p.LAST_NAME, p.EMAIL");
        $this->db->from("PEOPLE p");
        $this->db->where('p.PERSON_ID', $person_id);

        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Function to get all people
     *
     * @return mixed
     */
    public function get_all()
    {
        $this->db->select("p.PERSON_ID, p.FIRST_NAME, p.LAST_NAME, p.EMAIL");
        $this->db->from("PEOPLE p");
        $this->db->order_by("p.PERSON_ID");
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Function to delete a person
     *
     * @param $person_id
     */
    public function delete_entry($person_id)
    {
        $this->db->delete('PEOPLE', array('PERSON_ID' => $person_id));
    }

}