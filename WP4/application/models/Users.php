<?php

/**
 * Created by PhpStorm.
 * User: Sven Vanderwegen
 * Date: 3/05/2017
 * Time: 13:56
 */
class Users extends CI_Model
{
    public $username;
    public $password;

    public function __construct()
    {
        parent::__construct();

        // Load database library
        $this->load->database();
    }

    public function get_all()
    {
        $this->db->select("u.ID, u.USERNAME, u.PASSWORD, u.ACTIVE");
        $this->db->from("USERS u");

        $this->db->order_by("u.ID", "ASC");
        $this->db->where("u.ACTIVE", 1);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_entry($id)
    {
        $this->db->select("u.ID, u.USERNAME, u.PASSWORD, u.ACTIVE");
        $this->db->from("USERS u");

        $this->db->where("u.ACTIVE", 1);
        $this->db->where("u.ID", $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function get_entry_by_username($username)
    {
        $this->db->select("u.ID, u.USERNAME, u.PASSWORD, u.ACTIVE");
        $this->db->from("USERS u");

        $this->db->where("u.ACTIVE", 1);
        $this->db->where("u.USERNAME", $username);

        $query = $this->db->get();
        return $query->row();
    }

    public function insert_entry($username, $password)
    {
        $this->username = $username;
        $this->password = password_hash($password, PASSWORD_DEFAULT);

        $this->db->insert('USERS', $this);

        return $this->get_entry($this->db->insert_id());
    }

    public function verify_login($username, $password)
    {
        $user = $this->get_entry_by_username($username);
        if ($user){
            if (password_verify($password, $user->PASSWORD))
                return $user;
            else
                return false;
        } else {
            return false;
        }
    }
}