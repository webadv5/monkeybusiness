<?php

/**
 * Created by PhpStorm.
 * User: sven
 * Date: 3/05/2017
 * Time: 15:33
 */
class Events extends CI_Model
{

    public $event_name;
    public $person_id;
    public $start_date;
    public $end_date;

    public function __construct()
    {
        parent::__construct();

        // Load database library
        $this->load->database();
    }

    /**
     * Function to create an event
     */
    public function insert_entry($event_name, $person_id, $start_date, $end_date)
    {
        $this->event_name   = $event_name;
        $this->person_id    = $person_id;
        $this->start_date   = $start_date;
        $this->end_date     = $end_date;

        $this->db->insert('EVENTS', $this);
    }

    /**
     * Function to get an event based of its EVENT_ID
     *
     * @param $event_id
     * @return mixed
     */
    public function get_entry($event_id)
    {
        $this->db->select("e.EVENT_ID, e.EVENT_NAME, p.FIRST_NAME, p.LAST_NAME , e.START_DATE, e.END_DATE");
        $this->db->from("EVENTS e");
        $this->db->where('e.EVENT_ID', $event_id);

        $this->db->join(
            'PEOPLE p',
            'e.PERSON_ID = p.PERSON_ID',
            'left'
        );

        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Function to get all events
     *
     * @return mixed
     */
    public function get_all()
    {
        $this->db->select("e.EVENT_ID, e.EVENT_NAME, p.FIRST_NAME, p.LAST_NAME , e.START_DATE, e.END_DATE");
        $this->db->from("EVENTS e");

        $this->db->join(
            'PEOPLE p',
            'e.PERSON_ID = p.PERSON_ID',
            'left'
        );

        $this->db->order_by("e.EVENT_ID");
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Function to delete an event
     *
     * @param $event_id
     */
    public function delete_entry($event_id)
    {
        $this->db->delete('EVENTS', array('EVENT_ID' => $event_id));
    }
}