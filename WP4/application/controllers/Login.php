<?php

/**
 * Created by PhpStorm.
 * User: sven
 * Date: 3/05/2017
 * Time: 14:05
 */
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->login();
    }

    public function login()
    {
        $this->load->helper('form');

        $data["css"] = [
            "<link rel=\"stylesheet\" href=\"" . base_url("assets/css/login.css") . "\" >"
        ];

        $this->load->view('templates/header.php', $data);
        $this->load->view('login/login-form.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect("login");
    }

    public function register()
    {
        $this->load->helper('form');

        $data["css"] = [
            "<link rel=\"stylesheet\" href=\"" . base_url("assets/css/login.css") . "\" >"
        ];

        $this->load->view('templates/header.php', $data);
        $this->load->view('login/register-form.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function validation()
    {
        $this->load->library('form_validation');
        $this->load->model('users');

        $this->form_validation->set_rules('username', 'Gebruikersnaam', 'required|callback_check_credentials', [
            "required" => "Vul je gebruikersnaam in aub."
        ]);
        $this->form_validation->set_rules('password', 'Wachtwoord', 'required', [
            "required" => "Vul je wachtwoord in aub."
        ]);

        if ($this->form_validation->run()) {
            $this->session->set_userdata(["USER" => $this->users->get_entry_by_username($this->input->post('username'))]);
            redirect();
        } else {
            $this->login();
        }
    }

    public function register_validation()
    {
        $this->load->library('form_validation');
        $this->load->model('users');

        $this->form_validation->set_rules('username', 'Gebruikersnaam', 'required', [
            "required" => "Vul je gebruikersnaam in aub."
        ]);
        $this->form_validation->set_rules('password', 'Wachtwoord', 'required', [
            "required" => "Vul je wachtwoord in aub."
        ]);

        if ($this->form_validation->run()) {
            $this->users->insert_entry($this->input->post("username"), $this->input->post("password"));
            redirect();
        } else {
            $this->register();
        }
    }

    /**
     * Custom callback for the login form validation
     *
     * @return bool
     */
    public function check_credentials()
    {
        $this->load->model('users');
        if ($user = $this->users->verify_login($this->input->post('username'), $this->input->post('password'))) {
            return true;
        } else {
            $this->form_validation->set_message('check_credentials', 'Deze gebruikersnaam/wachtwoord combinatie bestaat niet');
            return false;
        }
    }

}