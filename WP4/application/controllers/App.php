<?php

/**
 * Created by PhpStorm.
 * User: sven
 * Date: 3/05/2017
 * Time: 14:03
 */
class App extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata("USER"))
            redirect("login");
    }

    public function index()
    {
        $this->load->helper("form");
        $this->load->model("events");
        $this->load->model("people");

        $data["css"] = [];
        $data["events"] = $this->events->get_all();
        $data["people"] = $this->people->get_all();

        $this->load->view('templates/header.php', $data);
        $this->load->view('app/index.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function create_event()
    {
        $this->load->model("events");
        $this->load->library("form_validation");

        $this->form_validation->set_message('required', 'Het veld "{field}" moet ingevuld zijn.');

        //region Form rules
        $this->form_validation->set_rules('event_name', 'evenement naam', 'required');
        $this->form_validation->set_rules('person_id', 'klant', 'required|integer');
        $this->form_validation->set_rules('start_date', 'start datum', 'required');
        $this->form_validation->set_rules('end_date', 'eind datum', 'required');
        //endregion

        if ($this->form_validation->run()) {

            $this->events->insert_entry(
                $this->input->post("event_name"),
                $this->input->post("person_id"),
                $this->input->post("start_date"),
                $this->input->post("end_date")
            );

            redirect();
        } else {
            $this->index();
        }
    }

    public function delete_event()
    {
        $this->load->model("events");
        if ($this->input->post("event_id"))
            $this->events->delete_entry($this->input->post("event_id"));
    }

    public function create_person()
    {
        $this->load->model("people");
        $this->load->library("form_validation");

        $this->form_validation->set_message('required', 'Het veld "{field}" moet ingevuld zijn.');

        //region Form rules
        $this->form_validation->set_rules('first_name', 'voornaam', 'required');
        $this->form_validation->set_rules('last_name', 'achternaam', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        //endregion

        if ($this->form_validation->run()) {

            $this->people->insert_entry(
                $this->input->post("first_name"),
                $this->input->post("last_name"),
                $this->input->post("email")
            );

            redirect();
        } else {
            $this->index();
        }
    }

    public function delete_person()
    {
        $this->load->model("people");
        if ($this->input->post("person_id"))
            $this->people->delete_entry($this->input->post("person_id"));
    }

}