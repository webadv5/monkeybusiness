<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Web Advanced - WP4</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#events">Evenementen</a></li>
                <li><a href="#people">Personen</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= base_url("login/logout") ?>">Logout</a></li>
                <li><a href="javascript:void(0)">Welkom <?= $this->session->userdata("USER")->USERNAME ?></a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">
    <div class="jumbotron">
        <h1>Web Advanced</h1>
        <p>Dit is de uitwerking van werkpakket 4 door groep 5</p>
    </div>

    <h2 id="events">Evenementen</h2>
    <hr/>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Evenement naam</th>
            <th>Klant</th>
            <th>Start datum</th>
            <th>Eind datum</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($events as $event): ?>
            <tr>
                <th scope="row"><?= $event->EVENT_ID ?></th>
                <td><?= $event->EVENT_NAME ?></td>
                <td><?= $event->FIRST_NAME . " " . $event->LAST_NAME ?></td>
                <td><?= $event->START_DATE ?></td>
                <td><?= $event->END_DATE ?></td>
                <td><span class="glyphicon glyphicon-trash event-delete" style="cursor: pointer" data-id="<?= $event->EVENT_ID ?>" aria-hidden="true"></span></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <h3>Een evenement aanmaken</h3>

    <hr/>
    <?= form_open("app/event/create") ?>
        <div class="form-group">
            <label for="inputEventName">Evenement naam</label>
            <input type="text" class="form-control" id="inputEventName" name="event_name" placeholder="Evenement naam">
        </div>
        <div class="form-group">
            <label for="inputKlant">Klant</label>
            <select class="form-control" name="person_id" id="inputKlant">
                <option value="" disabled selected>Kies een klant</option>
                <? foreach ($people as $person): ?>
                    <option value="<?= $person->PERSON_ID ?>"><?= $person->FIRST_NAME . " " . $person->LAST_NAME ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="inputStartDate">Start datum</label>
            <input type="datetime-local" name="start_date" class="form-control" id="inputStartDate" placeholder="Start datum">
        </div>
        <div class="form-group">
            <label for="inputEndDate">Eind datum</label>
            <input type="datetime-local" name="end_date" class="form-control" id="inputEndDate" placeholder="Eind datum">
        </div>

        <? if (form_error("event_name") OR form_error("person_id") OR form_error("start_date") OR form_error("end_date")): ?>
            <div class="alert alert-danger" role="alert">
                <?= form_error("event_name") ?>
                <?= form_error("person_id") ?>
                <?= form_error("start_date") ?>
                <?= form_error("end_date") ?>
            </div>
        <? endif ?>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>

    <br>
    <br>

    <h2 id="people">Personen</h2>
    <hr/>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Voornaam</th>
            <th>Achternaam</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($people as $person): ?>
            <tr>
                <th scope="row"><?= $person->PERSON_ID ?></th>
                <td><?= $person->FIRST_NAME ?></td>
                <td><?= $person->LAST_NAME ?></td>
                <td><?= $person->EMAIL ?></td>
                <td><span class="glyphicon glyphicon-trash person-delete" style="cursor: pointer" data-id="<?= $person->PERSON_ID ?>" aria-hidden="true"></span></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <h3>Een persoon aanmaken</h3>
    <hr/>
    <?= form_open("app/person/create") ?>
        <div class="form-group">
            <label for="inputFirstName">Voornaam</label>
            <input type="text" class="form-control" id="inputFirstName" name="first_name" placeholder="Voornaam">
        </div>
        <div class="form-group">
            <label for="inputLastName">Achternaam</label>
            <input type="text" class="form-control" id="inputLastName" name="last_name" placeholder="Achternaam">
        </div>
        <div class="form-group">
            <label for="inputEmail">Voornaam</label>
            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email">
        </div>

        <? if (form_error("first_name") OR form_error("last_name") OR form_error("email")): ?>
            <div class="alert alert-danger" role="alert">
                <?= form_error("first_name") ?>
                <?= form_error("last_name") ?>
                <?= form_error("email") ?>
            </div>
        <? endif ?>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>

    <br>
    <br>

</div>

<script>
    $(document).ready(function () {
        $(".person-delete").click(function () {

            var personId = $(this).data("id");
            var row = $(this).closest('tr')

            $.confirm({
                title: 'Verwijderen?',
                content: 'Wilt u deze klant verwijderen!',
                buttons: {
                    verwijder: function () {
                        $.ajax({
                            type: "POST",
                            url: window.location.origin + "/WP4/app/person/delete",
                            data: {"person_id": personId},
                            success: function () {
                                row.remove();
                            }
                        });
                    },
                    annuleer: function () {}
                }
            });
        });

        $(".event-delete").click(function () {

            var eventId = $(this).data("id");
            var row = $(this).closest('tr')

            $.confirm({
                title: 'Verwijderen?',
                content: 'Wilt u dit evenement verwijderen!',
                buttons: {
                    verwijder: function () {
                        $.ajax({
                            type: "POST",
                            url: window.location.origin + "/WP4/app/event/delete",
                            data: {"event_id": eventId},
                            success: function () {
                                row.remove();
                            }
                        });
                    },
                    annuleer: function () {}
                }
            });
        });
    });
</script>