<div class="container">
    <?= form_open("login/register_validation", ["class" => "form-signin"]) ?>
        <h2 class="form-signin-heading">Create a new user</h2>
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" id="inputUsername" class="form-control" placeholder="Username" name="username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">
            Register
        </button>
    </form>

    <?= validation_errors(); ?>
</div>