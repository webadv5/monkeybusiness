<?php

/**
 * Created by PhpStorm.
 * User: sven
 * Date: 18/04/2017
 * Time: 17:55
 */
class App_model extends CI_Model
{

    /**
     * App_model constructor.
     */
    public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    //------------------------------------------------------------------------------------------------------------------
    // EVENTS
    //------------------------------------------------------------------------------------------------------------------

    //region EVENTS
    /**
     * Function to create an event
     */
    public function create_event()
    {
        $input = array(
            'EVENT_NAME' => ucwords($this->input->post('EVENT_NAME')),
            'PERSON_ID' => $this->input->post('PERSON_ID'),
            'START_DATE' => $this->input->post('START_DATE'),
            'END_DATE' => $this->input->post('END_DATE'),
        );

        $this->db->insert('EVENTS', $input);
    }

    /**
     * Function to update an event
     */
    public function update_event()
    {
        $input = array(
            'EVENT_ID' => $this->input->post("EVENT_ID"),
            'EVENT_NAME' => ucwords($this->input->post('EVENT_NAME')),
            'PERSON_ID' => $this->input->post('PERSON_ID'),
            'START_DATE' => $this->input->post('START_DATE'),
            'END_DATE' => $this->input->post('END_DATE'),
        );

        $this->db->replace('EVENTS', $input);
    }

    /**
     * Function to get an event based of its EVENT_ID
     *
     * @param $EVENT_ID
     * @return mixed
     */
    public function get_event($EVENT_ID)
    {
        $this->db->select("e.EVENT_ID, e.EVENT_NAME, p.FIRST_NAME, p.LAST_NAME , e.START_DATE, e.END_DATE");
        $this->db->from("EVENTS e");
        $this->db->where('e.EVENT_ID', $EVENT_ID);

        $this->db->join(
            'PEOPLE p',
            'e.PERSON_ID = p.PERSON_ID',
            'left'
        );

        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Function to get all events
     *
     * @return mixed
     */
    public function get_events()
    {
        $this->db->select("e.EVENT_ID, e.EVENT_NAME, p.FIRST_NAME, p.LAST_NAME , e.START_DATE, e.END_DATE");
        $this->db->from("EVENTS e");

        $this->db->join(
            'PEOPLE p',
            'e.PERSON_ID = p.PERSON_ID',
            'left'
        );

        $this->db->order_by("e.EVENT_ID");
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Function to delete an event
     *
     * @param $EVENT_ID
     */
    public function delete_event($EVENT_ID)
    {
        $this->db->delete('EVENTS', array('EVENT_ID' => $EVENT_ID));
    }

    //endregion

    //------------------------------------------------------------------------------------------------------------------
    // PEOPLE
    //------------------------------------------------------------------------------------------------------------------

    //region PEOPLE

    /**
     * Function to create a person
     */
    public function create_person()
    {
        $input = array(
            'FIRST_NAME'    => ucwords(strtolower($this->input->post('FIRST_NAME'))),
            'LAST_NAME'     => ucwords(strtolower($this->input->post('LAST_NAME'))),
            'EMAIL'         => $this->input->post('EMAIL'),
        );

        $this->db->insert('PEOPLE', $input);
    }

    /**
     * Function to update a person
     */
    public function update_person()
    {
        $input = array(
            'PERSON_ID'     => $this->input->post("PERSON_ID"),
            'FIRST_NAME'    => ucwords(strtolower($this->input->post('FIRST_NAME'))),
            'LAST_NAME'     => ucwords(strtolower($this->input->post('LAST_NAME'))),
            'EMAIL'         => $this->input->post('EMAIL'),
        );

        $this->db->replace('PEOPLE', $input);
    }

    /**
     * Function to get a person based of its PERSON_ID
     *
     * @param $PERSON_ID
     * @return mixed
     */
    public function get_person($PERSON_ID)
    {
        $this->db->select("p.PERSON_ID, p.FIRST_NAME, p.LAST_NAME, p.EMAIL");
        $this->db->from("PEOPLE p");
        $this->db->where('p.PERSON_ID', $PERSON_ID);

        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Function to get all people
     *
     * @return mixed
     */
    public function get_people()
    {
        $this->db->select("p.PERSON_ID, p.FIRST_NAME, p.LAST_NAME, p.EMAIL");
        $this->db->from("PEOPLE p");
        $this->db->order_by("p.PERSON_ID");
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Function to delete a person
     *
     * @param $PERSON_ID
     */
    public function delete_person($PERSON_ID)
    {
        $this->db->delete('PEOPLE', array('PERSON_ID' => $PERSON_ID));
    }

    //endregion

}