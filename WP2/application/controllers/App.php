<?php

/**
 * Created by PhpStorm.
 * User: sven
 * Date: 18/04/2017
 * Time: 17:42
 */
class App extends CI_Controller
{

    /**
     * App constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Defaul page for the controller
     */
    public function index()
    {
        $this->events();
    }

    /**
     * Page to view all events
     */
    public function events()
    {
        //region Loading
        $this->load->model("app_model");
        $this->load->helper("form");
        //endregion

        $data["EVENTS"] = $this->app_model->get_events();

        $this->load->view("templates/header.php");
        $this->load->view("app/homepage.php", $data);
        $this->load->view("templates/footer.php");
    }

    /**
     * Page to view all people
     */
    public function people()
    {
        //region Loading
        $this->load->model("app_model");
        $this->load->helper("form");
        //endregion

        $data["PEOPLE"] = $this->app_model->get_people();

        $this->load->view("templates/header.php");
        $this->load->view("app/people.php", $data);
        $this->load->view("templates/footer.php");
    }

    /**
     * Page to add a event
     */
    public function add_event()
    {
        //region Loading
        $this->load->model("app_model");
        $this->load->helper("form");
        //endregion

        //region Queries
        $data["EVENTS"] = $this->app_model->get_events();
        $data["PEOPLE"] = $this->app_model->get_people();
        //endregion

        $this->load->view("templates/header.php");
        $this->load->view("app/create-event.php", $data);
        $this->load->view("templates/footer.php");
    }

    /**
     * Page to add a person
     */
    public function add_person()
    {
        //region Loading
        $this->load->model("app_model");
        $this->load->helper("form");
        //endregion

        $this->load->view("templates/header.php");
        $this->load->view("app/create-person.php");
        $this->load->view("templates/footer.php");
    }

    /**
     * Page to modify a person
     *
     * @param $id
     */
    public function modify_person($id)
    {
        //region Loading
        $this->load->model("app_model");
        $this->load->helper("form");
        //endregion

        $data["PERSON"] = $this->app_model->get_person($id);

        $this->load->view("templates/header.php");
        $this->load->view("app/modify-person.php", $data);
        $this->load->view("templates/footer.php");
    }

    //region Form validation

    /**
     * Form validation for $this->add_event()
     */
    public function create_event()
    {
        $this->load->library('form_validation');
        $this->load->model('app_model');

        //region Form rules
        $this->form_validation->set_rules('EVENT_NAME', 'EVENT_NAME', 'required');
        $this->form_validation->set_rules('PERSON_ID', 'PERSON_ID', 'required|integer');
        $this->form_validation->set_rules('START_DATE', 'START_DATE', 'required');
        $this->form_validation->set_rules('END_DATE', 'END_DATE', 'required');
        //endregion

        if ($this->form_validation->run()) {
            $this->app_model->create_event();
            redirect('app/event');
        } else {
            $this->add_event();
        }
    }

    /**
     * Form validation for $this->add_person()
     */
    public function create_person()
    {
        $this->load->library('form_validation');
        $this->load->model('app_model');

        //region Form rules
        $this->form_validation->set_rules('FIRST_NAME', 'FIRST_NAME', 'required');
        $this->form_validation->set_rules('LAST_NAME', 'LAST_NAME', 'required');
        $this->form_validation->set_rules('EMAIL', 'EMAIL', 'required');
        //endregion

        if ($this->form_validation->run()) {
            $this->app_model->create_person();
            redirect('app/person');
        } else {
            $this->add_person();
        }
    }

    /**
     * Form validation for $this->modify_person()
     */
    public function update_person()
    {
        $this->load->library('form_validation');
        $this->load->model('app_model');

        //region Form rules
        $this->form_validation->set_rules('PERSON_ID', 'PERSON_ID', 'required|integer');
        $this->form_validation->set_rules('FIRST_NAME', 'FIRST_NAME', 'required');
        $this->form_validation->set_rules('LAST_NAME', 'LAST_NAME', 'required');
        $this->form_validation->set_rules('EMAIL', 'EMAIL', 'required');
        //endregion

        if ($this->form_validation->run()) {
            $this->app_model->update_person();
            redirect('app/person');
        } else {
            $this->modify_person();
        }
    }

    //endregion

    //region Delete items

    /**
     * Page to delete a event
     *
     * @param $id
     */
    public function delete_event($id)
    {
        $this->load->model('app_model');
        $this->app_model->delete_event($id);
    }

    /**
     * Page to delete a person
     *
     * @param $id
     */
    public function delete_person($id)
    {
        $this->load->model('app_model');
        $this->app_model->delete_person($id);
    }

    //endregion

}