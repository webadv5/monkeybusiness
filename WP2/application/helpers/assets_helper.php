<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('DS', DIRECTORY_SEPARATOR);


if (!function_exists('asset_url'))
{
    function asset_url($string = "")
    {
        return base_url() . 'assets/' . $string;
    }
}
