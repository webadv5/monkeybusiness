<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('clean'))
{
    function clean($string = "")
    {
        return htmlspecialchars($string,ENT_QUOTES, 'UTF-8');
    }
}
