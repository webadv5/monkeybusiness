<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Web Advanced</title>
    <link rel="stylesheet" href="<?= asset_url("css/layout.css") ?>">
    <script src="//use.fontawesome.com/e627fdcd08.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
</head>
<body>

<div id="global-sidebar" class="sidebar">
    <nav>
        <div class="sidebar-title" style="margin-top: 20px; border: none;">App</div>
        <a href="<?php echo base_url("app/event"); ?>"><i class="fa fa-calendar" aria-hidden="true"></i>Evenementen</a>
        <a href="<?php echo base_url("app/person"); ?>"><i class="fa fa-user" aria-hidden="true"></i>Personen</a>
    </nav>
</div>

<div id="main">
    <header class="global"></header>
