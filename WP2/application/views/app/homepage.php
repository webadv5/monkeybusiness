<div class="context">
    <div class="block">
        <div class="block-title">Opvragen<h1>Evenementen</h1></div>
        <div class="block-icons"><a href="<?= base_url("app/event/create") ?>"><i class="fa fa-plus" aria-hidden="true"></i></a></div>
        <div class="block-content" style="text-align: center">
            <table width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>EVENT_NAME</th>
                    <th>PERSON_ID</th>
                    <th>START_DATE</th>
                    <th>END_DATE</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($EVENTS as $EVENT): ?>
                    <tr>
                        <td><?php echo clean($EVENT->EVENT_ID) ?></td>
                        <td><?php echo clean($EVENT->EVENT_NAME); ?></td>
                        <td><?php echo clean($EVENT->FIRST_NAME) . " " . clean($EVENT->LAST_NAME); ?></td>
                        <td><?php echo date("d-m-Y", strtotime(clean($EVENT->START_DATE))); ?></td>
                        <td><?php echo date("d-m-Y", strtotime(clean($EVENT->END_DATE))); ?></td>
                        <td>
                            <i class="fa fa-trash" data-id="<?= clean($EVENT->EVENT_ID) ?>" aria-hidden="true"></i>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".fa-trash").click(function () {
            var id = $(this).data("id");
            $.ajax({
                url: window.location.origin + "/app/event/" + id + "/delete",
                method: 'GET',
                success: function () {
                    location.reload();
                }
            });
        });
    });
</script>