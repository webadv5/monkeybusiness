<div class="context">
    <div class="block">
        <div class="block-title">
            Aanmaken <h1>Evenement</h1>
        </div>
        <div class="block-content">
            <?= form_open("app/event/modify/submit") ?>
            <input type="text" id="EVENT_ID" class="form-control" name="EVENT_ID" value="<?= $EVENT->EVENT_ID; ?>">
            <div class="grid">
                <div class="grid__col grid__col--1-of-2">
                    <div class="form-group">
                        <label for="EVENT_NAME" >Evenement naam</label>
                        <input type="text" id="EVENT_NAME" class="form-control" name="EVENT_NAME" value="<?= $EVENT->EVENT_NAME; ?>">
                    </div>
                    <div class="form-group">
                        <label for="PERSON_ID" >Person</label>
                        <select id="PERSON_ID" class="form-control" name="PERSON_ID">
                            <option value="" disabled selected>Kies een persoon</option>
                            <? foreach ($PEOPLE as $PERSON): ?>
                                <option value="<?= $PERSON->PERSON_ID ?>" <?= set_select('PERSON_ID', $PERSON->PERSON_ID) ?>><?= $PERSON->FIRST_NAME . " " . $PERSON->LAST_NAME ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="grid__col grid__col--1-of-2">
                    <div class="form-group">
                        <label for="START_DATE" >Start datum</label>
                        <input type="datetime-local" id="START_DATE" class="form-control" name="START_DATE" value="<?= set_value('START_DATE'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="END_DATE" >Eind datum</label>
                        <input type="datetime-local" id="END_DATE" class="form-control" name="END_DATE" value="<?= set_value('END_DATE'); ?>">
                    </div>
                </div>
            </div>

            <br>
            <input type="submit">
            <input type="reset">
            <?= form_close() ?>
        </div>

        <? if (validation_errors()): ?>
            <div class="alerts">
                <div class="alert" id="">
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                    <strong>Error!</strong><?= validation_errors() ?>
                </div>
            </div>
        <? endif ?>

    </div>
</div>