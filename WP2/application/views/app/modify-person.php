<div class="context">
    <div class="block">
        <div class="block-title">
            Aanmaken <h1>Persoon</h1>
        </div>
        <div class="block-content">
            <?= form_open("app/person/modify/submit") ?>
            <input type="hidden" name="PERSON_ID" value="<?= $PERSON->PERSON_ID ?>">
            <div class="grid">
                <div class="grid__col grid__col--1-of-2">
                    <div class="form-group">
                        <label for="FIRST_NAME" >FIRST_NAME</label>
                        <input type="text" id="FIRST_NAME" value="<?= $PERSON->FIRST_NAME ?>" class="form-control" name="FIRST_NAME">
                    </div>
                    <div class="form-group">
                        <label for="LAST_NAME" >LAST_NAME</label>
                        <input type="text" id="LAST_NAME" class="form-control" value="<?= $PERSON->LAST_NAME ?>" name="LAST_NAME">
                    </div>
                </div>
                <div class="grid__col grid__col--1-of-2">
                    <div class="form-group">
                        <label for="EMAIL" >EMAIL</label>
                        <input type="email" id="EMAIL" class="form-control" value="<?= $PERSON->EMAIL ?>" name="EMAIL">
                    </div>
                </div>
            </div>
            <?= validation_errors() ?>
            <input type="submit">
            <input type="reset">
            <?= form_close() ?>
        </div>
    </div>

    <? if (validation_errors()): ?>
        <div class="alerts">
            <div class="alert" id="">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                <strong>Error!</strong><?= validation_errors() ?>
            </div>
        </div>
    <? endif ?>

</div>