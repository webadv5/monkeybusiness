<div class="context">
    <div class="block">
        <div class="block-title">Opvragen<h1>Personen</h1></div>
        <div class="block-icons"><a href="<?= base_url("app/person/create") ?>"><i class="fa fa-plus" aria-hidden="true"></i></a></div>
        <div class="block-content" style="text-align: center">
            <table width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($PEOPLE as $PERSON): ?>
                    <tr>
                        <td><?php echo clean($PERSON->PERSON_ID) ?></td>
                        <td><?php echo clean($PERSON->FIRST_NAME) . " " . clean($PERSON->LAST_NAME); ?></td>
                        <td><?php echo clean($PERSON->EMAIL); ?></td>
                        <td>
                            <a href="<?= base_url("app/person/" . $PERSON->PERSON_ID . "/modify") ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <i class="fa fa-trash" data-id="<?= clean($PERSON->PERSON_ID) ?>"  aria-hidden="true"></i>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $(".fa-trash").click(function () {
            var id = $(this).data("id");
            $.ajax({
                url: window.location.origin + "/app/person/" + id + "/delete",
                method: 'GET',
                success: function () {
                    location.reload();
                }
            });
        });
    });
</script>