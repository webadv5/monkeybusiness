<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="Style_WP3.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


</head>
<body>
<p id="output"></p>
<h1>    Events</h1>
<table id="table" class="col-2">
    <tr class="headerTable">
        <th>ID</th>
        <th>Name</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th></th>
    </tr>
</table>

<script type="text/javascript">

    function deleteSelected(a) {
        fetch('https://webadv.be/api/events/' + a, {method: "DELETE"}).then(function () {
            window.location.reload(false);

        });
    }
</script>
<script type="text/javascript">
    window.onload = function getData() {
        fetch('https://webadv.be/api/events',
            {
                method: "get", headers: new Headers(
                {'Content-Type': 'text/plain'})
            })
            .then(function (response) {
                //$('#output').html(response.json());
                return response.json();
            }).then(function (data) {
            var table = document.getElementById('table');
            for (var i = 0; i < data.length; i++) {
                var id = data[i].eventID;
                var Row = document.createElement('tr');
                Row.innerHTML =
                    '<td>' + data[i].eventID + '</td>' +
                    '<td>' + data[i].eventName + '</td>' +
                    '<td>' + data[i].startDate + '</td>' +
                    '<td>' + data[i].endDate + '</td>' +
                    '<td> <input type="button" value="delete" id="btn' + data[i].eventID + '" ></td>'; //onclick="deleteSelected()"

                table.appendChild(Row);
                document.getElementById("btn" + id).addEventListener("click", function () {
                    deleteSelected(id);
                }, false);
            }

        }).catch(function (err) {
            alert(err.message);
        });
    };

</script>
</body>

</html>