<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 17-5-2017
 * Time: 15:18
 */


namespace Api;

use Api\Event;
use Api\EventRepository;
use PHPUnit\Framework\TestCase;

require "../vendor/autoload.php";

class EventRepositoryTest extends TestCase
{
    private $mockPDO;
    private $mockPDOStatement;

    public function setUp()
    {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
    }

    public function tearDown()
    {
        unset($this->mockPDO);
        unset($this->mockPDOStatement);
    }

    public function testFindEvenementById_idExists_PersonObject()
    {
        $evenement = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    'EVENT_ID' => $evenement->getEventID(),
                    'EVENT_NAME' => $evenement->getEventName(),
                    'PERSON_ID' => $evenement->getPersonID(),
                    'START_DATE' => $evenement->getStartDate(),
                    'END_DATE' => $evenement->getEndDate(),
                ]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsById($evenement->getEventID());

        $this->assertEquals($evenement, $actualEvenement);
    }

    public function testFindEventById_idDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsById(24);

        $this->AssertNull($actualEvenement);
    }

    public function testFindEvents_Exists_EventsObject()
    {
        $evenement1 = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');
        $evenement2 = new Event(2, 'Nieuwe timers', '2', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $allEvents = [$evenement1, $evenement2];

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [[
                    'EVENT_ID' => $evenement1->getEventID(),
                    'EVENT_NAME' => $evenement1->getEventName(),
                    'PERSON_ID' => $evenement1->getPersonID(),
                    'START_DATE' => $evenement1->getStartDate(),
                    'END_DATE' => $evenement1->getEndDate(),
                ],
                [
                    'EVENT_ID' => $evenement2->getEventID(),
                    'EVENT_NAME' => $evenement2->getEventName(),
                    'PERSON_ID' => $evenement2->getPersonID(),
                    'START_DATE' => $evenement2->getStartDate(),
                    'END_DATE' => $evenement2->getEndDate(),
                ]]
                ));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenementen = $pdoRepository->getEvents();

        $this->assertEquals($allEvents, $actualEvenementen);
    }

    public function testFindEvents_noEvents_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEvents();

        $this->assertNull($actualEvenement);
    }

    public function testFindEventByCustomer_customerExists_EventObject()
    {
        $evenement = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [[
                    'EVENT_ID' => $evenement->getEventID(),
                    'EVENT_NAME' => $evenement->getEventName(),
                    'PERSON_ID' => $evenement->getPersonID(),
                    'START_DATE' => $evenement->getStartDate(),
                    'END_DATE' => $evenement->getEndDate(),
                ]]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsByPersonId($evenement->getPersonID());

        $this->assertEquals([$evenement], $actualEvenement);
    }

    public function testFindEventByCustomer_customerDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsByPersonId(20);

        $this->assertNull($actualEvenement);
    }

    public function testFindEventByDates_datesExists_EventObject()
    {
        $evenement = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [[
                    'EVENT_ID' => $evenement->getEventID(),
                    'EVENT_NAME' => $evenement->getEventName(),
                    'PERSON_ID' => $evenement->getPersonID(),
                    'START_DATE' => $evenement->getStartDate(),
                    'END_DATE' => $evenement->getEndDate(),
                ]]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsByDate($evenement->getStartDate(), $evenement->getEndDate());

        $this->assertEquals([$evenement], $actualEvenement);
    }

    public function testFindEventByDates_datesDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsByDate( '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->assertNull($actualEvenement);
    }

    public function testFindEventByCustomerAndDates_customerAndDatesExists_EventObject()
    {
        $evenement = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [[
                    'EVENT_ID' => $evenement->getEventID(),
                    'EVENT_NAME' => $evenement->getEventName(),
                    'PERSON_ID' => $evenement->getPersonID(),
                    'START_DATE' => $evenement->getStartDate(),
                    'END_DATE' => $evenement->getEndDate(),
                ]]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement =
            $pdoRepository->getEventsByPersonIdAndDate($evenement->getPersonID() ,$evenement->getStartDate(), $evenement->getEndDate());

        $this->assertEquals([$evenement], $actualEvenement);
    }

    public function testFindEventByCustomerAndDates_customerAndDatesDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new EventRepository($this->mockPDO);
        $actualEvenement = $pdoRepository->getEventsByPersonIdAndDate(1 , '2017-03-30 15:13:30', '2017-04-01 15:13:36');
        $this->assertNull($actualEvenement);
    }

    public function testInsertEvent_newEvent_eventObject(){

        $evenement = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new EventRepository($this->mockPDO);

        $actualEvenement = $pdoRepository->createEvent('OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->assertEquals($actualEvenement, true);
    }


    public function testUpdateEvent_event_eventObject(){
        $evenement = new Event(1, 'OldTimers', '1', '2017-03-30 15:13:30', '2017-04-01 15:13:36');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new EventRepository($this->mockPDO);
        $pdoRepository->updateEvent(1, 'OldTimers', '1', '2016-03-30 15:13:30', '2017-04-01 15:13:36');
        $actualEvenement = $pdoRepository->getEventsById(1);
        $this->assertNotEquals($evenement,$actualEvenement);
    }

    public function testdeleteEvent_event_null(){

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new EventRepository($this->mockPDO);
        $pdoRepository->createEvent(1, 'OldTimers', '1', '2016-03-30 15:13:30', '2017-04-01 15:13:36');
        $rowcount = $pdoRepository->deleteEvent(1);
        $actualEvenement = $pdoRepository->getEvents();
        $this->assertNull($actualEvenement);
    }

}
