<?php

require_once __DIR__ . "/vendor/autoload.php";
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
$router = new AltoRouter();

$config = file_get_contents('parser.json') or die("Parser doesn't exist.");
$config = json_decode($config) ;

##$database = "mysql:host=localhost;dbname=webadv_db1";
##$user = "webadv_sqluser";
##$password = "i[(yTv]55xAD";

$router->setBasePath("/api");

$router->map('GET','/', function () {
    echo "Hello World";
});

$router->map('GET','/events', function () use ($config) {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));
    $array = $event_repository->getEvents();
    echo json_encode($array, JSON_PRETTY_PRINT);
});

$router->map('GET','/events/?', function () use ($config) {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));
    $array = $event_repository->getEventsByDate($_GET["from"], $_GET["until"]);
    echo json_encode($array, JSON_PRETTY_PRINT);
});

$router->map('GET','/events/[i:id]', function ($id) use ($config) {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));
    $array =  $event_repository->getEventsById($id);
    echo json_encode($array, JSON_PRETTY_PRINT);
});

$router->map('GET','/events/person/[i:person_id]', function ($person_id) use ($config) {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));
    $array =  $event_repository->getEventsByPersonId($person_id);
    echo json_encode($array, JSON_PRETTY_PRINT);
});

$router->map('GET','/person/[i:person_id]/events/?', function ($person_id) use ($config) {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));
    $array =  $event_repository->getEventsByPersonIdAndDate($person_id, $_GET["from"], $_GET["until"]);
    echo json_encode($array, JSON_PRETTY_PRINT);
});

$router->map('POST','/events', function () use ($config){
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));

    if (isset($_POST["event_name"]) AND $_POST["person_id"] AND $_POST["start_date"] AND $_POST["end_date"])
    {
        $output = $event_repository->createEvent(
            $_POST["event_name"],
            $_POST["person_id"],
            $_POST["start_date"],
            $_POST["end_date"]
        );

        echo json_encode($output);

    } else
    {
        http_response_code(400);
        echo json_encode(["error" => "Missing arguments"]);
    }
});

$router->map('DELETE','/events/[i:id]', function ($id) use ($config){
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));

    $output = $event_repository->deleteEvent(
        $id
    );

    echo json_encode(["Affected rows" => $output]);
});

$router->map('PUT','/events/[i:id]', function ($id) use ($config){
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $event_repository = new Api\EventRepository(new \PDO($config->database, $config->username, $config->password));
    parse_str(file_get_contents("php://input"), $PUT);

    if (isset($PUT["event_name"]) AND $PUT["person_id"] AND $PUT["start_date"] AND $PUT["end_date"])
    {
        $output = $event_repository->updateEvent(
            $id,
            $PUT["event_name"],
            $PUT["person_id"],
            $PUT["start_date"],
            $PUT["end_date"]
        );

        echo json_encode($output);

    } else
    {
        http_response_code(400);
        echo json_encode(["error" => "Missing arguments"]);
    }
});



$match = $router->match();

if( $match && is_callable( $match['target'] ) ){
    call_user_func_array( $match['target'], $match['params'] );
}
