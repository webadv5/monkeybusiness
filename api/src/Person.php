<?php
namespace Api;

class Person {

    private $personID;
    private $firstName;
    private $lastName;

    /**
     * Person constructor.
     * @param $personID
     * @param $firstName
     * @param $lastName
     */
    public function __construct($personID, $firstName, $lastName)
    {
        $this->personID = $personID;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getPersonID()
    {
        return $this->personID;
    }

    /**
     * @param mixed $personID
     */
    public function setPersonID($personID)
    {
        $this->personID = $personID;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }




}