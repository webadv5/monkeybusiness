<?php
namespace Api;


class Event implements \JsonSerializable
{
    private $eventID;
    private $eventName;
    private $personID;
    private $startDate;
    private $endDate;

    /**
     * Event constructor.
     * @param $eventID
     * @param $eventName
     * @param $personID
     * @param $startDate
     * @param $endDate
     */
    public function __construct($eventID, $eventName, $personID, $startDate, $endDate)
    {
        $this->eventID = $eventID;
        $this->eventName = $eventName;
        $this->personID = $personID;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getEventID()
    {
        return $this->eventID;
    }

    /**
     * @param mixed $eventID
     */
    public function setEventID($eventID)
    {
        $this->eventID = $eventID;
    }

    /**
     * @return mixed
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * @param mixed $eventName
     */
    public function setEventName($eventName)
    {
        $this->eventName = $eventName;
    }

    /**
     * @return mixed
     */
    public function getPersonID()
    {
        return $this->personID;
    }

    /**
     * @param mixed $personID
     */
    public function setPersonID($personID)
    {
        $this->personID = $personID;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }




}