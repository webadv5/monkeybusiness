<?php
namespace Api;

class EventRepository
{
    private $pdo = null;

    function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Gets all events from the database
     *
     * @return array
     */
    function getEvents()
    {
        $output = array();

        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM EVENTS');
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();

            $output = [];
            $return = $statement->fetchAll();

            if ($return !== null) {
                foreach ($return as $row) {
                    array_push($output, new Event($row["EVENT_ID"], $row["EVENT_NAME"], $row["PERSON_ID"], $row["START_DATE"], $row["END_DATE"]));
                }
            }

            if (count($output) > 0) {
                return $output;
            } else {
                return null;
            }
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }


    function createEvent($event_name, $person_id, $start_date, $end_date)
    {

        try
        {
           // $pdo = new \PDO("mysql:host=localhost;dbname=" . $this->database, $this->user, $this->password);

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("INSERT INTO `EVENTS` (`EVENT_ID`, `EVENT_NAME`, `PERSON_ID`, `START_DATE`, `END_DATE`) 
                VALUES (NULL, :event_name, :person_id, :start_date, :end_date);");

            $statement->bindParam(':event_name', $event_name, \PDO::PARAM_STR);
            $statement->bindParam(':person_id', $person_id, \PDO::PARAM_INT);
            $statement->bindParam(':start_date', $start_date, \PDO::PARAM_STR);
            $statement->bindParam(':end_date', $end_date, \PDO::PARAM_STR);

            $statement->execute();
            return true;
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }

    function updateEvent($event_id, $event_name, $person_id, $start_date, $end_date)
    {
        try
        {
           // $pdo = new \PDO("mysql:host=localhost;dbname=" . $this->database, $this->user, $this->password);

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("UPDATE `EVENTS` SET 
                `EVENT_NAME` = :event_name, 
                `PERSON_ID` = :person_id, 
                `START_DATE` = :start_date, 
                `END_DATE` = :end_date 
                WHERE `EVENTS`.`EVENT_ID` = :id;");

            $statement->bindParam(':id', $event_id, \PDO::PARAM_INT);
            $statement->bindParam(':event_name', $event_name, \PDO::PARAM_INT);
            $statement->bindParam(':person_id', $person_id, \PDO::PARAM_STR);
            $statement->bindParam(':start_date', $start_date, \PDO::PARAM_STR);
            $statement->bindParam(':end_date', $end_date, \PDO::PARAM_STR);

            $statement->execute();
            return $this->getEventsById($event_id);
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }

    function deleteEvent($event_id)
    {
        try
        {
        //    $pdo = new \PDO("mysql:host=localhost;dbname=" . $this->database, $this->user, $this->password);

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("DELETE FROM `EVENTS` WHERE `EVENT_ID` = :event_id;");

            $statement->bindParam(':event_id', $event_id, \PDO::PARAM_STR);

            $statement->execute();
            return $statement->rowCount();
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }


    /**
     * Gets an event by id from the database
     *
     * @param $id
     * @return Event
     */
    function getEventsById($id)
    {
        try
        {
         //   $pdo = new \PDO("mysql:host=localhost;dbname=" . $this->database, $this->user, $this->password);

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM EVENTS WHERE EVENT_ID = ?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $row = $statement->fetchAll();

            if (count($row) > 0) {
                $output = new Event($row["EVENT_ID"], $row["EVENT_NAME"], $row["PERSON_ID"], $row["START_DATE"], $row["END_DATE"]);
            } else {
                $output = null;
            }

            return $output;
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }

        return null;
    }

    /**
     * Gets an event by person_id from the database
     *
     * @param $person_id
     * @return array
     */
    function getEventsByPersonId($person_id)
    {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM EVENTS WHERE PERSON_ID = ?');
            $statement->bindParam(1, $person_id, \PDO::PARAM_INT);
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();

            $output = [];

            $return = $statement->fetchAll();

            foreach ($return as $row) {
                array_push($output, new Event($row["EVENT_ID"], $row["EVENT_NAME"], $row["PERSON_ID"], $row["START_DATE"], $row["END_DATE"]));
            }

            if (count($output) > 0) {
                return $output;
            } else {
                return null;
            }
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }

    /**
     * Gets all events between two dates from the database
     *
     * @param $start_date
     * @param $end_date
     * @return array|null
     */
    function getEventsByDate($start_date, $end_date)
    {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM EVENTS WHERE START_DATE >= ? AND END_DATE <= ?');
            $statement->bindParam(1, $start_date, \PDO::PARAM_STR );
            $statement->bindParam(2, $end_date, \PDO::PARAM_STR );
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();

            $output = [];

            $return = $statement->fetchAll();

            foreach ($return as $row) {
                array_push($output, new Event($row["EVENT_ID"], $row["EVENT_NAME"], $row["PERSON_ID"], $row["START_DATE"], $row["END_DATE"]));
            }

            if (count($output) > 0) {
                return $output;
            } else {
                return null;
            }
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }


    function getEventsByPersonIdAndDate($person_id, $start_date, $end_date)
    {
        try
        {
        //    $pdo = new \PDO("mysql:host=localhost;dbname=" . $this->database, $this->user, $this->password);

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM EVENTS WHERE START_DATE >= ? AND END_DATE <= ? AND PERSON_ID = ?');
            $statement->bindParam(1, $start_date, \PDO::PARAM_STR );
            $statement->bindParam(2, $end_date, \PDO::PARAM_STR );
            $statement->bindParam(3, $person_id, \PDO::PARAM_INT);
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();

            $output = [];

            $return = $statement->fetchAll();

            foreach ($return as $row) {
                array_push($output, new Event($row["EVENT_ID"], $row["EVENT_NAME"], $row["PERSON_ID"], $row["START_DATE"], $row["END_DATE"]));
            }

            if (count($output) > 0) {
                return $output;
            } else {
                return null;
            }
        }
        catch (\PDOException $e)
        {
            print 'exception !: ' . $e->getMessage();
        }
        return null;
    }
}

